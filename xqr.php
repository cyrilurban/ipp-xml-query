<?php
/**
 * @file	xqr.php
 * @author	CYRIL URBAN
 * @date:	2017-03-08
 * @brief	XQR
 */
 
	// set parameters
	$parameters = setParameters($argv, $argc);

	// load input data
	if (isset($parameters["input"])) {
		$inputXmlData = openFile($parameters["input"]);
	} else {
		$inputXmlData = file_get_contents("php://stdin");
	}
	
	// parse words to query words (and check syntax)
	$queryWords = parseQuery($parameters["query"]);

	// find result with query words in the input XML data
	$outputXmlData = executeQuery($inputXmlData, $queryWords, $parameters);

	// write output data
	if (isset($parameters["output"])) {
		file_put_contents($parameters["output"], $outputXmlData);
	} else {
		print($outputXmlData);
	}
	
	// and successfully end (return 0)
	exit(0);



	/**
	 * ExecuteQuery function produces output data from input data and
	 * parameters. Uses a helper function filter.
	 *
	 * @param	  $inXml	   The input XML data
	 * @param	  $queryWords  The query words
	 * @param	  $parameters  The parameters
	 *
	 * @return	 $outStringData  Output data in format string
	 */
	function executeQuery($inXml, $queryWords, $parameters) {
		
		// transfare input data to DOMnode
		$xml = new DOMDocument();
		$xml->loadXML($inXml);

		// filtering XML input dat 
		$filterData = filter($queryWords, $xml);
		
		// formating data to nicely indentation
		$filterData->formatOutput = true;

		// transfare DOMnode to string
		$outStringData = $filterData->saveHTML();
		
		// add root
		if (isset($parameters["root"])) {
			
			//empty root
			if(strpos($outStringData, "<") === FALSE) {
				$outStringData = ("<" . $parameters["root"] . "/>" );
			}
			else{
				$outStringData = ("<" . $parameters["root"] . ">\n" . "$outStringData" ."</" . $parameters["root"] . ">" );
			}
		}

		// add XML head
		if (!isset($parameters["n"])) {
			 $outStringData = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" . "$outStringData"; 
		}
		
		return $outStringData;
	}


	/**
	 * The function filters the XML data according to query words and returns
	 * this data in DOM format (XML).
	 *
	 * @param	  $queryWords  The query words
	 * @param	  $xml		 The xml
	 *
	 * @return	 $outputXmlData  The output XML data
	 */
	function filter($queryWords, $xml) {

		// output XML data : DOMnode
		$outputXmlData = new DOMDocument();

		$select = $queryWords["SELECT"];

		// FROM
		if ((isset($queryWords["FROM"]["ID"][0]))) {
			// FROM = element.
			if ((!empty($queryWords["FROM"]["ID"][0]))) {
				$from = $queryWords["FROM"]["ID"][0];
			}
			else {
				// FROM = root
				$from=$xml->documentElement->nodeName;		
			}	
		}
		else {
			// FROM = element
			$from = $queryWords["FROM"]["VALUE"];
		}

		// WHERE
		if ((isset($queryWords["WHERE"]["ID"][0]))) {
			// WHERE = element.
			if ((!empty($queryWords["WHERE"]["ID"][0]))) {
				$where = $queryWords["WHERE"]["ID"][0];
			}
		}
		else {
			// WHERE = element
			$where = $queryWords["WHERE"]["VALUE"];
		}

		// operator = CONTAINS || '<' || '>' || '='
		$operator = $queryWords["WHERE"]["REL.OPER"];

		// literal
		$literal = $queryWords["WHERE"]["LITERAL"]["STRING"];
		if ((isset($queryWords["WHERE"]["LITERAL"]["STRING"]))) {
			$literal = $queryWords["WHERE"]["LITERAL"]["STRING"];
		}
		else if ((isset($queryWords["WHERE"]["LITERAL"]["NUMBER"]))) {
			$literal = $queryWords["WHERE"]["LITERAL"]["NUMBER"];
		}

		// limit
		if ((isset($queryWords["LIMIT"]))) {
			$limit = $queryWords["LIMIT"];
		}
		else {
			$limit = PHP_INT_MAX;
		}
		$countLimit = 0; 



		// FROM != ROOT
		if ($queryWords["FROM"]["VALUE"] != "ROOT") {
			
			// generate all 'elemsFrom'
			$elemsFrom = $xml->getElementsByTagName($from);
			
			// element -> choose first
			if ($queryWords["FROM"]["TYPE"] == "ELEMENT") {	
				$elemFrom = $elemsFrom->item(0);
			}
			// element.attribute -> choose first with attribute
			else if ($queryWords["FROM"]["TYPE"] =="FULL") {
				
				$i = 0;
				foreach ($elemsFrom as $elemFrom) {	

					if ($elemFrom->hasAttribute($queryWords["FROM"]["ID"][1])) {
						$elemFrom = $elemsFrom->item($i);
						$find = TRUE;
						break;
					}
					$i++;
				}

				if($find != TRUE) {
					return $outputXmlData;
				}

			}
			// .attribute -> choose first with attribute
			else {
				$att = $queryWords["FROM"]["ID"][1];
				$wordWithAtt = findAtt($att, $xml);
			
				// re-generate
				$elemsFrom = $xml->getElementsByTagName($wordWithAtt);

				$i = 0;
				foreach ($elemsFrom as $elemFrom) {	

					if ($elemFrom->hasAttribute($queryWords["FROM"]["ID"][1])) {
						$elemFrom = $elemsFrom->item($i);
						$find = TRUE;
						break;
					}
					$i++;
				}

				if($find != TRUE) {
					return $outputXmlData;
				}
			}	
		}	


		// SELECT FROM ROOT or FROM 'from'
		if ($queryWords["FROM"]["VALUE"] == "ROOT") {
			$elemsSelect = $xml->getElementsByTagName($select);
		}
		else {
			$elemsSelect = $elemFrom->getElementsByTagName($select);
			
		}

		// SELECT searching
		foreach ($elemsSelect as $elemSelect) {	

			// WHERE exist
			if ((isset($queryWords["WHERE"]))) {

				// .attribute in WHERE 1/2 . Hack! Skipping forcycle - WHERE.
				if (($queryWords["WHERE"]["TYPE"] == "ATTRIBUTE") ||
					($select == $where)) {
					$elemsWhere = array(0);
				}
				else {
					$elemsWhere = $elemSelect->getElementsByTagName($where);	
				}

				foreach ($elemsWhere as $elemWhere) {	
					


					// .attribute in WHERE 2/2 . Hack! Skipping forcycle - WHERE.
					if (($queryWords["WHERE"]["TYPE"] == "ATTRIBUTE") ||
						($select == $where)){
						$elemWhere = $elemSelect;
					}
					
					// WHERE = element
					if ($queryWords["WHERE"]["TYPE"] == "ELEMENT") {
						$elemOrAtt = $elemWhere->nodeValue;
					}
					else {
						// WHERE = element.attribute
						$elemOrAtt = $elemWhere->getAttribute($queryWords["WHERE"]["ID"][1]);

					}

					// literal is STIRNG
					if (isset($queryWords["WHERE"]["LITERAL"]["STRING"])) {
						
						// operator CONTAINS
						if($operator == "CONTAINS") {
							
							
							// YES (=!NOT) && LITERAL 
							if ( (strstr($elemOrAtt, $literal) != false) and
								($queryWords["WHERE"]["NOT/YES"] == "YES")
								) {
								// LIMIT
								if($countLimit < $limit) {
									$newElement = $outputXmlData->importNode($elemSelect, true);
									$outputXmlData->appendChild($newElement);
								}
								$countLimit++;	
							}
							// NOT && LITERAL
							else if (($queryWords["WHERE"]["NOT/YES"] == "NOT") && 
								(strstr($elemOrAtt, $literal) == false)) {
								// LIMIT
								if($countLimit < $limit) {
									$newElement = $outputXmlData->importNode($elemSelect, true);
									$outputXmlData->appendChild($newElement);
								}
								$countLimit++;
							}
						}
						// operator = (strings are the same)
						else if($operator == "=") {
							// YES (=!NOT) && LITERAL 
							if (($queryWords["WHERE"]["NOT/YES"] == "YES") && 
								(strcmp($elemOrAtt, $literal) == 0)) {
								// LIMIT
								if($countLimit < $limit) {
									$newElement = $outputXmlData->importNode($elemSelect, true);
									$outputXmlData->appendChild($newElement);
								}
								$countLimit++;								
							}
							// NOT && LITERAL
							else if (($queryWords["WHERE"]["NOT/YES"] == "NOT") && 
								(strcmp($elemOrAtt, $literal) != 0)) {
								// LIMIT
								if($countLimit < $limit) {
									$newElement = $outputXmlData->importNode($elemSelect, true);
									$outputXmlData->appendChild($newElement);
								}
								$countLimit++;
							}								
						}
						// operator < (literal < "string")
						else if($operator == "<") {
							// YES (=!NOT) && LITERAL 
							if (($queryWords["WHERE"]["NOT/YES"] == "YES") && 
								(strcmp($elemOrAtt, $literal) == -1)) {
								// LIMIT
								if($countLimit < $limit) {
									$newElement = $outputXmlData->importNode($elemSelect, true);
									$outputXmlData->appendChild($newElement);
								}
								$countLimit++;									
							}
							// NOT && LITERAL
							else if (($queryWords["WHERE"]["NOT/YES"] == "NOT") && 
								(strcmp($elemOrAtt, $literal) != -1)) {
								// LIMIT
								if($countLimit < $limit) {
									$newElement = $outputXmlData->importNode($elemSelect, true);
									$outputXmlData->appendChild($newElement);
								}
								$countLimit++;
							}
						}
						// operator > (literal > "string")
						else if($operator == ">") {
							// YES (=!NOT) && LITERAL 
							if (($queryWords["WHERE"]["NOT/YES"] == "YES") && 
								(strcmp($elemOrAtt, $literal) == 1)) {
								// LIMIT
								if($countLimit < $limit) {
									$newElement = $outputXmlData->importNode($elemSelect, true);
									$outputXmlData->appendChild($newElement);
								}
								$countLimit++;									
							}
							// NOT && LITERAL
							else if (($queryWords["WHERE"]["NOT/YES"] == "NOT") && 
								(strcmp($elemOrAtt, $literal) != 1)) {
								// LIMIT
								if($countLimit < $limit) {
									$newElement = $outputXmlData->importNode($elemSelect, true);
									$outputXmlData->appendChild($newElement);
								}
								$countLimit++;
							}
						}
					}

					// literal is a NUMBER
					else {
						// operator =
						if($operator == "=") {	
							// YES (=!NOT) && LITERAL 
							if (($queryWords["WHERE"]["NOT/YES"] == "YES") &&
							 ($elemOrAtt == $literal) ) {
								// LIMIT
								if($countLimit < $limit) {
									$newElement = $outputXmlData->importNode($elemSelect, true);
									$outputXmlData->appendChild($newElement);
								}
								$countLimit++;
							}
							// NOT && LITERAL 
							else if (($queryWords["WHERE"]["NOT/YES"] == "NOT") && ($elemOrAtt != $literal) ) {
								// LIMIT
								if($countLimit < $limit) {
									$newElement = $outputXmlData->importNode($elemSelect, true);
									$outputXmlData->appendChild($newElement);
								}
								$countLimit++;
							}
						}
						// operator <
						else if($operator == "<") {
							// YES (=!NOT) && LITERAL 
							if (($queryWords["WHERE"]["NOT/YES"] == "YES") &&
							 ($elemOrAtt < $literal) ) {
								// LIMIT
								if($countLimit < $limit) {
									$newElement = $outputXmlData->importNode($elemSelect, true);
									$outputXmlData->appendChild($newElement);
								}
								$countLimit++;
							}
							// NOT && LITERAL 
							else if (($queryWords["WHERE"]["NOT/YES"] == "NOT") && ($elemOrAtt > $literal) ) {
								// LIMIT
								if($countLimit < $limit) {
									$newElement = $outputXmlData->importNode($elemSelect, true);
									$outputXmlData->appendChild($newElement);
								}
								$countLimit++;
							}
						}
						// operator >
						else if($operator == ">") {
							// YES (=!NOT) && LITERAL 
							if (($queryWords["WHERE"]["NOT/YES"] == "YES") &&
							 ($elemOrAtt > $literal) ) {
								// LIMIT
								if($countLimit < $limit) {
									$newElement = $outputXmlData->importNode($elemSelect, true);
									$outputXmlData->appendChild($newElement);
								}
								$countLimit++;
							}
							// NOT && LITERAL 
							else if (($queryWords["WHERE"]["NOT/YES"] == "NOT") && ($elemOrAtt < $literal) ) {
								// LIMIT
								if($countLimit < $limit) {
									$newElement = $outputXmlData->importNode($elemSelect, true);
									$outputXmlData->appendChild($newElement);
								}
								$countLimit++;
							}
						}
					}
				}
			}
			// WHERE is empty
			else {
				// LIMIT
				if($countLimit < $limit) {
					$newElement = $outputXmlData->importNode($elemSelect, true);
					$outputXmlData->appendChild($newElement);
				}
				$countLimit++;
			}
		}
	
		return $outputXmlData;
	}


	/**
	 * Function finds the attribute in the tree from root. TODO; transfare to
	 * recurse.
	 *
	 * @param	  $att	The finding attribute
	 * @param	  $xml	The imput XML (root)
	 *
	 * @return	 The name of node which has got finding argument
	 */
	function findAtt($att, $xml) {
				
		foreach($xml->childNodes as $a) { 
			if ($a->nodeName != "#text") {

				if ($a->hasAttribute($att)) {
					$result = $a->nodeName;
					return $result;
				}
			}

			if ($a->hasChildNodes()) {
				
				foreach($a->childNodes as $b) {
					
					if ($b->nodeName != "#text") {

						if ($b->hasAttribute($att)) {
							$result = $b->nodeName;
							return $result;
						}
					}
					
					if ($b->hasChildNodes()) {

						foreach($b->childNodes as $c) {

							if ($c->nodeName != "#text") {
								if ($c->hasAttribute($att)) {
									$result = $c->nodeName;
									return $result;
								}
							}

							if ($c->hasChildNodes()) {

								foreach($c->childNodes as $d) {

									if ($d->nodeName != "#text") {
										if ($d->hasAttribute($att)) {
											$result = $d->nodeName;
											return $result;
										}
									}
								}
							}
						}
					}
				}
			}
		}	
	}


	/**
	 * Parsing the sentence to words. Check syntax and semantic control. It is
	 * based on the principle of lexical, syntactic and semantic checks (like in
	 * the course IFJ). Regular expressions are using to filter words sometimes.
	 *
	 * @param	  $data   The sentence
	 *
	 * @return	 $words	It is array of parsing words
	 */
	function parseQuery($data)
	{
		// add whitespace next to operators
		$data = preg_replace("/</", " < " , $data);
		$data = preg_replace("/=/", " = " , $data);
		$data = preg_replace("/>/", " > " , $data);
		$data = preg_replace("/CONTAINS/", " CONTAINS " , $data);

		// parse to words
		$parts = preg_split('/\s+/', $data);

		// delete last empty key from array
		if(empty($parts[count($parts)-1])) {
			unset($parts[count($parts)-1]);
		}

		// 'i' = possition in the array, i++ something like getToken
		$i = 0;


		if(count($parts) < 3) {
			errorMessage("Syntax or semantic error!\n", 80);
		}

		// SELECT
		if ($parts[0] == "SELECT") {
			$words["SELECT"] = $parts[1];
		}
		else {
			errorMessage("Syntax or semantic error!\n", 80);
		}

		// FROM
		if ($parts[2] == "FROM") {
			// <FROM-ELM> = (emty)
			if(($parts[3] == "WHERE") || ($parts[3] == "LIMIT") || (count($parts) == 3)) {
				$words["FROM"]["EMPTY"] = "(empty)";
				$i = 3;
			}
			else {
				// <FROM-ELM> = <ELEMENT-OR-ATTRIBUTE> or ROOT
				$words["FROM"]["EMPTY"] = "notEmpty";
				
				if(preg_match('/\.{1}/', $parts[3])) {
					
					$words["FROM"]["ID"] = preg_split("/\./", $parts[3]);	
					// <ELEMENT-OR-ATTRIBUTE> = ATRIBUTE || ELEMENT.ATTRIBUTE (=FULL)
					if (empty($words["FROM"]["ID"][0])) {
						$words["FROM"]["TYPE"] = "ATRIBUTE";
					}
					else {
						$words["FROM"]["TYPE"] = "FULL";
					}
				}
				// <ELEMENT-OR-ATTRIBUTE> = ELEMENT
				else {
					$words["FROM"]["TYPE"] = "ELEMENT";
					$words["FROM"]["VALUE"] = $parts[3];
				}
				
				$i = 4;
			}
		}
		else {
			errorMessage("Syntax or semantic error!\n", 80);
		}


		if(count($parts) == 3 || count($parts) == 4) {
			return($words);
		}

		// WHERE
		if($parts[$i] == "WHERE") {
			// <CONDITION> --> NOT
			if($parts[$i+1] == "NOT") {
				$words["WHERE"]["NOT/YES"] = "NOT";
				$i++;
				// NOT NOT = YES
				for ($y=0; $i<count($parts); $y++) {
					if($parts[$i+1] == "NOT") {
						if($words["WHERE"]["NOT/YES"] == "YES") {
							$words["WHERE"]["NOT/YES"] = "NOT";
						}
						else {
							$words["WHERE"]["NOT/YES"] = "YES";
						}
						$i++;
					}
					else {
						break;
					}
				}
			}
			else {
				$words["WHERE"]["NOT/YES"] = "YES";
			}
		
			$i++;
			
			// <ELEMENT-OR-ATTRIBUTE>
			if(preg_match('/\.{1}/', $parts[$i])) {
				// split
				$words["WHERE"]["ID"] = preg_split("/\./", $parts[$i]);	
				// <ELEMENT-OR-ATTRIBUTE> = ATRIBUTE || ELEMENT.ATTRIBUTE (=FULL)
				if (empty($words["WHERE"]["ID"][0])) {
					$words["WHERE"]["TYPE"] = "ATTRIBUTE";
				}
				else {
					$words["WHERE"]["TYPE"] = "FULL";
				}
			}

			// <ELEMENT-OR-ATTRIBUTE> = ELEMENT
			else {
				$words["WHERE"]["TYPE"] = "ELEMENT";
				$words["WHERE"]["VALUE"] = $parts[$i];
			}
			
			$i++;

			// <RELATION-OPERATOR>
			if( ($parts[$i] == "CONTAINS") || ($parts[$i] == "=") || ($parts[$i] == ">") || ($parts[$i] == "<")){
				$words["WHERE"]["REL.OPER"] = $parts[$i];
				$i++;
			}
			else {
				errorMessage("Syntax or semantic error!\n", 80);
			}

			// <LITERAL> --> number
			if (is_numeric($parts[$i])) { 
				// contains other chars then numbers
				if(preg_match("/[^0-9]/", $parts[$i])) {
					errorMessage("Syntax or semantic error!\n", 80);
				}
				$words["WHERE"]["LITERAL"]["NUMBER"] = $parts[$i];	  
				(int)$words["WHERE"]["LITERAL"]["NUMBER"];
				// after CONTAINS can not be a number
				if($words["WHERE"]["REL.OPER"] == "CONTAINS") {
					errorMessage("Syntax or semantic error!\n", 80);
				}
 			}

 			// <LITERAL> --> string
 			else if(preg_match('/"(.*)"/', $parts[$i])) {
 				// "" on the wrong possition (then on first or last possition)
 				if((substr($parts[$i], -1) != '"') || (substr($parts[$i], 0, 1) != '"')) {
 					errorMessage("Syntax or semantic error!\n", 80);
 				}
 				
 				$words["WHERE"]["LITERAL"]["STRING"] = str_replace('"', '', $parts[$i]);
 				// more then two "
 				if (((strlen($words["WHERE"]["LITERAL"]["STRING"]))+2 != strlen($parts[$i]))) {
 					errorMessage("Syntax or semantic error!\n", 80);
 				}
 			}
 			else {
 				errorMessage("Syntax or semantic error!\n", 80);
 			}
			$i++;
		}
			
		// LIMIT
		if($parts[$i] == "LIMIT") {
			$i++;
			// must be number
			if (is_numeric($parts[$i]) || $parts[$i] == 0) { 
				// contains other chars then numbers
				if(preg_match("/[^0-9]/", $parts[$i])) {
					errorMessage("Syntax or semantic error!\n", 80);
				}
				$words["LIMIT"] = $parts[$i]; 			   
				(int)$words["LIMIT"];
				// LIMIT can not be smaller then 0
				if ((int)$words["LIMIT"] < 0) {
					errorMessage("Syntax or semantic error!\n", 80);
				}
 			}
 			else {
 				errorMessage("Syntax or semantic error!\n", 80);
 			}	
		}
		else {
			// different word then LIMIT
			if((isset($parts[$i])) == 1){
				errorMessage("Syntax or semantic error!\n", 80);
			}	
		}

		// last test
		if($i < count($parts)-1) {
			errorMessage("Syntax or semantic error!\n", 80);
		}
	
		return($words);
	}

	/**
	 * Opens a file.
	 *
	 * @param	 $file   The file
	 *
	 * @return	$readedFile
	 */
	function openFile($file)
	{
		if (file_exists($file)) {
			// error code 2
			$opendFile = fopen($file, "r") or errorMessage("This file can not be opened.\n", 2);
			$readedFile = fread($opendFile, filesize($file));
			fclose($opendFile);
			return $readedFile;
		} 
		else { 
			// error code 2
			errorMessage("This file does not exist\n", 2);
		}
	}

	/**
	 * Sets the parameters from arguments.
	 *
	 * @param	  $argv   
	 * @param	  $argc  
	 *
	 * @return	 array $parameters
	 */
	function setParameters($argv, $argc) {
		
		for($i = 1; $i < $argc; $i++){

			// --help
			if($argv[$i] == "--help") {
				// --help must be first and only argument
				if (($argv[1] == "--help") && ($argc == 2)){
						printHelpMessage();
						exit(0);
				}
				// error code 1
				errorMessage("--help must be first and only argument\n", 1);
			}

			// --input
			if(preg_match("/--input=(.*)$/", $argv[$i]))
			{
				$parameters["input"] = preg_replace("/--input=(.*)$/", "$1" , $argv[$i]);
			}

			// --output
			if(preg_match("/--output=(.*)$/", $argv[$i]))
			{
				$parameters["output"] = preg_replace("/--output=(.*)$/", "$1" , $argv[$i]);
			}

			// --query
			if(preg_match("/--query=(.*)$/", $argv[$i]))
			{
				$parameters["query"] = preg_replace("/--query=(.*)$/", "$1" , $argv[$i]);
			}

			// --qf
			if(preg_match("/--qf=(.*)$/", $argv[$i]))
			{
				$parameters["qf"] = preg_replace("/--qf=(.*)$/", "$1" , $argv[$i]);
			}

			// -n
			if($argv[$i] == "-n") {
				$parameters["n"] = "TRUE";
			}

			// --root
			if(preg_match("/--root=(.*)$/", $argv[$i]))
			{
				$parameters["root"] = preg_replace("/--root=(.*)$/", "$1" , $argv[$i]);
			}
		}

		// wrong parameters, error code 1
		if ( ($argc-1 != count($parameters)) 
			|| (isset($parameters["query"]) == isset($parameters["qf"]))
			) {
			errorMessage("Wrong parameters! Please use --help.\n", 1);
		}

		if (isset($parameters["qf"])) {
			$parameters["query"] = openFile($parameters["qf"]);
		}

		return($parameters);
	}

	/**
	 * The function print error message
	 *
	 * @param	  string $mess
	 * @param	  int  $exitCode
	 */
	function errorMessage($mess, $exitCode)
	{
		file_put_contents("php://stderr", $mess);
		exit($exitCode);
	}

	/**
	 * Print help message
	 */
	function printHelpMessage() {
		print (
				"The script performs the task, similar to SELECT statement from SQL on XML file. \n\n".
				"Script works with these parameters: \n".
				"--help\t\t\tPrint help message\n".
				"--input=filename\tInput XML file\n".
				"--output=filename\tOutput XML file\n".
				"--query=task\t\tEntered task\n".
				"--qf=filename\t\tEntered task from file, can't be combinated with --query=task\n".
				"-n\t\t\tDon't generate XML head on output file\n".
				"--root=element\t\tName of XML root. If not specified, results aren't wrapped around the root element.\n"
			);
	}
	
?>
